# Project-Wide Copyright Information

This file tells copyright and license information that applies to the whole project of Super Practica.

The copyrights to contributions are held by their contributors. See the [AUTHORS.md](AUTHORS.md) file for the list of Super Practica contributors.

See the top-level LICENSES folder for the licenses used in this project.


## Code

Source code is in general licensed under the [GNU Affero General Public License](LICENSES/AGPL-3.0) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Some code files might be licensed under different terms. The "SPDX-License-Identifier" near the top of the file will tell what license it is in that case.

Third party packages will contain their copyright and license information within their own folders.


## Assets

Assets such as images and audio are in general licensed under the [Creative Commons Attribution-ShareAlike 4.0 International Public License](LICENSES/CC-BY-SA-4.0).

In exceptional cases, there will be a correspondingly named ".license" file with copyright and license information for that specific file.


## Wiki and Documents

All documents published in connection with Super Practica and all work on the wiki are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International Public License](LICENSES/CC-BY-SA-4.0), except where stated otherwise.


## Website

Website text and images are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International Public License](LICENSES/CC-BY-SA-4.0), except where stated otherwise.

Website source code is licensed under the [Apache License 2.0](LICENSES/Apache-2.0).
