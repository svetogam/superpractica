# Super Practica

## Overview

[Super Practica](https://superpractica.org) is a revolutionary game designed to optimize building expertise in mathematics. It is free and open-source software built with [Godot](https://godotengine.org/).

It's kind of hard to explain though. The [Blueprint](https://codeberg.org/superpractica/blueprint) gives an outline of its theory and design.


## How to play the game

* [Through the website](https://superpractica.org/play)
* [Through Codeberg](https://codeberg.org/superpractica/superpractica/releases)


## How to edit the game

1. Download the source code to get the Godot project folder.

2. Download [Godot Engine 3.5.3](https://godotengine.org/download/archive/3.5.3-stable/). (Some versions will be compatible and some won't be.)

3. Open the project folder in Godot.

The game should be playable and editable in Godot. Please tell me if you followed the instructions and it's not working.


## How to contribute

For an overview of different ways to contribute, see the [Contribute page](https://superpractica.org/contribute) on the website.

To contribute modifications through Godot, see [CONTRIBUTING.md](CONTRIBUTING.md).

To contribute funds, go to [Liberapay](https://liberapay.com/SuperPractica/) or [Patreon](https://www.patreon.com/superpractica).

For discussing Super Practica outside of particular issues, visit the [contributor "forum"](https://codeberg.org/superpractica/discussion).


## Copyright

See [COPYRIGHT.md](COPYRIGHT.md) for copyright information.

