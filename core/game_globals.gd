##############################################################################
# This file is part of Super Practica.                                       #
# Copyright (c) 2023 Super Practica contributors                             #
#----------------------------------------------------------------------------#
# See the COPYRIGHT.md file at the top-level directory of this project       #
# for information on the license terms of Super Practica as a whole.         #
#----------------------------------------------------------------------------#
# SPDX-License-Identifier: AGPL-3.0-or-later                                 #
##############################################################################

class_name GameGlobals

const NO_OBJECT := -1
const NO_TOOL := -1

const RELEASE_TAG := "v0.6.0"
const SOURCE_URL := "https://codeberg.org/superpractica/superpractica"

const COLOR_HIGHLIGHT := Color(1.0, 1.0, 0.5)
const COLOR_WARNING := Color(1.0, 0.0, 0.0)
const COLOR_AFFIRMATION := Color(0.6, 1.0, 0.6)
const COLOR_GUIDE := Color(0.5, 0.5, 1.0)
